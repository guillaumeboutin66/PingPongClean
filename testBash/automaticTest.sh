#!/bin/bash

#Color for display
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'


#Start of the test
echo "==== TEST DU PONG Symfony ===="



echo -e "\n++ Verification Initialisation ++"

countRst=$(curl -s --cookie cookies.txt --cookie-jar cookies.txt  $1/count | jq '.pingCount')

if [ 0 = $countRst ]
then
    echo -e "Initialisation: ${GREEN}OK${NC}"
else
    echo -e "${RED}Erreur dans l'initalisation ${NC}"
    echo "La variable attendue est : 0"
    echo "Le resultat est : " $countRst
fi




echo -e "\n++ Verification Ping ++"

pongRst=$(curl -s --cookie cookies.txt --cookie-jar cookies.txt  $1/ping | jq '.message')

if [ '"pong"' = $pongRst ]
then
    echo -e "Message ping:  ${GREEN}OK${NC}"
else
    echo -e "${RED}Erreur du résultat Json${NC}"
    echo "La variable attendue est : pong"
    echo "Le resultat est : " $pongRst
fi





echo -e "\n++ Verification Incrémentation ping ++"

countRst=$(curl -s --cookie cookies.txt --cookie-jar cookies.txt  $1/count | jq '.pingCount')

if [ 1 = $countRst ]
then
    echo -e "Increment Ping: ${GREEN}OK${NC}"
else
    echo -e "${RED}Erreur dans l'incrémentation${NC}"
    echo "La variable attendue est : 0"
    echo "Le resultat est : " $countRst
fi




echo -e "\n++ Verification Incrémentation Aléatoire ++"

Rand=$[ ( $RANDOM % 50 )  + 1 ]

for i in `seq 1 $Rand`;
do
	curl -s --cookie cookies.txt --cookie-jar cookies.txt  $1/ping >/dev/null
done

countRst=$(curl -s --cookie cookies.txt --cookie-jar cookies.txt  $1/count | jq '.pingCount')

if [ $((Rand + 1 )) = $countRst ]
then
    echo -e "Increment Ping: ${GREEN}OK${NC}"
else
    echo -e "${RED} Erreur dans l'incrémentation$${NC}"
    echo "La variable attendue est : " $Rand
    echo "Le resultat est : " $countRst
fi

echo -e "\n"

rm cookies.txt
