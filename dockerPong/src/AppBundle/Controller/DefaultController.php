<?php

namespace AppBundle\Controller;

use Raven_Client;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

use Sentry\SentryBundle\Event\SentryUserContextEvent;
use Sentry\SentryBundle\SentrySymfonyEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;



class DefaultController extends Controller
{


    public function pingAction(Request $request)
    {
        $session = $request->getSession();
        if($session->get('user_id')==null){
            $this->startSession($request);
        }

        $count = $session->get('user_id')+1;
        $session->set('user_id', $count);

        return $this->render('default/ping.html.twig', []);
    }

    public function countAction(Request $request)
    {
        $session = $request->getSession();
        if($session->get('user_id')==null){
            $this->startSession($request);
        }

        $userId = $session->get('user_id');

        return $this->render('default/count.html.twig', array('count' => $userId ));
    }

    private function startSession(Request $request){
        $sentryClient = new Raven_Client('https://e1048a181692451eb236af2c5c492a48:1429ec4f84e84c46a565aa94c39f32ea@sentry.io/257497');
        $session = $request->getSession();
        $session->set('user_id', 0);

        $sentryClient->captureMessage('New session has been started !', array('level' => 'debug'));
    }

}
